<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::post('/login/custom', [
    'uses' => 'LoginController@login',
    'as' => 'login.custom'
]);


//Resource
Auth::routes();


//user
Route::group(['middleware' => ['auth' => 'user']], function () {
    Route::resource('pengajuan', 'PengajuanController');
    Route::resource('logbook', 'IsiLogbookController');
    Route::resource('magang', 'MagangController');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/error_ditolak', 'MagangController@errorPenolakan')->name('error_ditolak');
    Route::get('/error_pengajuan_diproses', 'PengajuanController@errorDiproses')->name('error_pengajuan_diproses');
    Route::get('/error_pengajuan', 'PengajuanController@sudahPengajuan')->name('error_pengajuan');
    Route::get('/error_belum_magang', 'MagangController@error_belum_diterima_magang')->name('error_status');
    Route::get('/error_sudah_diterima', 'MagangController@error_sudah_diterima_magang')->name('error_diterima');
});

//admin
Route::group(['middleware' => ['auth' => 'admin']], function () {
    Route::resource('penilaian', 'PenilaianController');
    Route::get('/home_dashboard', function () {
        return view('admin/home_admin');
    })->name('admin_dashboard');
    Route::get('/list_pengajuan_admin', 'PengajuanController@index_admin')->name('list_pengajuan_admin');
    Route::get('/edit_pengajuan_admin/{id}', 'PengajuanController@edit_pengajuan_admin')->name('edit_pengajuan_admin');
    Route::post('/update_pengajuan_admin/{id}', 'PengajuanController@update_admin')->name('update_pengajuan_admin');
    Route::post('/tolak_pengajuan/{id}', 'PengajuanController@tolak_admin')->name('tolak_pengajuan');
    Route::post('/terima_pengajuan/{id}', 'PengajuanController@terima_ropeg_admin')->name('terima_pengajuan');
    Route::post('/teruskan_kabiro/{id}', 'PengajuanController@teruskan_kabiro')->name('teruskan_kabiro');
    Route::post('/terima_magang/{id}', 'PengajuanController@terima_magang_kabag')->name('terima_magang');
    Route::get('/list_magang_admin', 'MagangController@index_admin')->name('list_magang_admin');
    Route::get('/list_logbook_admin', 'IsiLogbookController@index_admin')->name('list_logbook_admin');
    Route::post('/acc_logbook_admin/{id}', 'IsiLogbookController@acc_logbook')->name('acc_logbook');
    Route::post('/tolak_logbook_admin/{id}', 'IsiLogbookController@tolak_logbook')->name('tolak_logbook');
    Route::resource('tambahadmin', 'TambahAdminController');

    Route::get('/create_penilaian/{id}', 'PenilaianController@create_admin')->name('tambah_penilaian');
    Route::post('/tambah_penilaian/{id}', 'PenilaianController@store_admin')->name('simpan_penilaian');
    Route::post('/selesaikan_magang/{id}', 'MagangController@selesaikan_magang')->name('selesaikan_magang');
});





// guest
Route::get('/alur_pengajuan', function () {
    return view('/user/alur_pengajuan');
})->name('alur_pengajuan');
Route::get('/tentang_simagang', function () {
    return view('/user/tentang_simagang');
})->name('tentang_simagang');

//lain
Route::get('/file/{id}', 'PengajuanController@openFile')->name('open_file');
