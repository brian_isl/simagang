<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bagian extends Model
{
    protected $guarded = ['id_biro', 'nama_bagian'];

    public function bagian_biro(){
        return $this->belongsTo('App\Biro', 'id_biro', 'id');
    }

    public function bagian_pengguna(){
        return $this->hasMany('App\User', 'id_bagian', 'id');
    }

    public function bagian_magang(){
        return $this->hasMany('App\Magang', 'id_bagian', 'id');
    }
}
