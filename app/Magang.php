<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Magang extends Model
{
    protected $fillable =
    [
        'id_bagian', 'id_nilai', 'id_pengajuan', 'status_magang',
    ];

    protected $hidden =
    [
        'created_at', 'updated_at'
    ];

    protected $cast =
    [

    ];

    public function magang_pengajuan()
    {
        return $this->belongsTo('App\Pengajuan', 'id_pengajuan', 'id');
    }

    // public function magang_pengguna()
    // {
    //     return $this->belongsTo('App\User', 'id_pengguna', 'id');
    // }

    // public function magang_biro()
    // {
    //     return $this->belongsTo('App\Biro', 'id_biro', 'id');
    // }

    public function magang_bagian()
    {
        return $this->belongsTo('App\Bagian', 'id_bagian', 'id');
    }

    public function magang_logbook()
    {
        return $this->hasMany('App\Logbook', 'id_magang', 'id');
    }

    public function magang_nilai()
    {
        return $this->hasOne('App\Nilai', 'id_magang', 'id');
    }
}
