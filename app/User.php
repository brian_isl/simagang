<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'id_biro', 'id_bagian',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pengguna(){
        return $this->hasMany('App\Pengajuan', 'id_pengguna', 'id');
    }

    // public function pengguna_magang(){
    //     return $this->hasOne('App\Magang', 'id_pengguna', 'id');
    // }

    // public function pengguna_logbook()
    // {
    //     return $this->hasMany('App\Logbook', 'id_pengguna', 'id');
    // }

    public function pengguna_biro()
    {
        return $this->belongsTo('App\Biro', 'id_biro', 'id');
    }

    public function pengguna_bagian(){
        return $this->belongsTo('App\Bagian', 'id_bagian', 'id');
    }

    public function is_admin(){
        if($this->is_admin){
            return true;
        }
        else{
            return false;

        }
    }
}
