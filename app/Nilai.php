<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{

    protected $fillable =
    [
        'id_magang', 'kedisiplinan', 'kinerja', 'komunikasi', 'etika', 'nilai_akhir',
    ];

    protected $hidden =
    [
        'created_at', 'updated_at'
    ];

    protected $cast =
    [

    ];
    public function nilai_magang()
    {
        return $this->belongsTo('App\Magang', 'id_magang', 'id');
    }


}
