<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    protected $fillable = [
    'nama_pengaju', 'id_pengguna', 'asal_surat', 'no_surat',' tgl_surat', 'perihal',
    'teruskan_biro', 'teruskan_bagian', 'tgl_masuk', 'keterangan', 'file_pengajuan', 'status', 'jumlah_anggota',
    'no_induk', 'no_telp', 'email', 'jurusan', 'mulai_magang', 'selesai_magang', 'nama_anggota1',
    'no_induk1', 'email1', 'no_telp1', 'nama_anggota2',
    'no_induk2', 'email2', 'no_telp2',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $cast = [
        'tgl_surat' => 'date', 'tgl_masuk' => 'date'
    ];

    public function biro_pembaca(){
        return $this->belongsTo('App\Biro', 'teruskan_biro', 'id');
    }

    public function pengaju(){
        return $this->belongsTo('App\User', 'id_pengguna', 'id');
    }

    public function pengajuan_magang(){
        return $this->hasOne('App\Magang', 'id_pengajuan', 'id');
    }

}
