<?php

namespace App\Http\Controllers;

use App\Magang;
use App\Nilai;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function create_admin($id)
    {
        $magang = Magang::find($id);
        return view('admin/tambah_penilaian_admin', ['nilai' => $magang]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    public function store_admin(Request $request, $id)
    {
        $magang = Magang::find($id);

        Nilai::Create([
            'id_magang' => $magang->id,
            'kedisiplinan' => $request->input('kedisiplinan'),
            'kinerja' => $request->input('kinerja'),
            'komunikasi' => $request->input('komunikasi'),
            'etika' => $request->input('etika'),
            'nilai_akhir' => ($request->input('kedisiplinan')*0.25)+($request->input('kinerja')*0.25)
            +($request->input('komunikasi')*0.25)+($request->input('etika')*0.25),
        ]);

        return redirect()->route('list_magang_admin');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $magang = Magang::find($id);
        return view('admin/tambah_penilaian_admin', ['nilai' => $magang]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect()->route('list_magang_admin');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
