<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengajuan;
use App\Magang;



class PengajuanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pengajuan = Pengajuan::where(['id_pengguna' => auth()->user()->id])->get();
        return view('user/list_pengajuan', ['pengajuan' => $pengajuan]);
    }

    public function index_admin()
    {

        if (auth()->user()->id_biro == 1 && auth()->user()->id_bagian == null) {
            $pengajuan = Pengajuan::all();
        }
        else if (auth()->user()->id_biro != 1 && auth()->user()->id_bagian == null){
            $pengajuan = Pengajuan::where(['teruskan_biro' => auth()->user()->id_biro])->get();
        }
        else if(auth()->user()->id_bagian != null){
            $pengajuan = Pengajuan::where(['teruskan_bagian' => auth()->user()->id_bagian])->get();
        }
        return view('admin/list_pengajuan_admin', ['pengajuan' => $pengajuan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pengajuan = Pengajuan::with(['pengajuan_magang'])->where(['id_pengguna' => auth()->user()->id])->first();
        if ($pengajuan['status'] == null || $pengajuan['status'] == 0 || $pengajuan->pengajuan_magang['status_magang'] == 2) {
            return view('user/form_pengajuan', ['pengajuan' => $pengajuan]);

        }
        else{
            return redirect()->route('error_pengajuan_diproses');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = uniqid() . '_pengajuan_' . $request->input('nama_pengaju') . '.pdf';

        Pengajuan::create([
            'id_pengguna' => auth()->user()->id,
            'nama_pengaju' => $request->input('nama_pengaju'),
            'no_induk' => $request->input('no_induk'),
            'no_telp' => $request->input('no_telp'),
            'email' => auth()->user()->email,
            'asal_surat' => $request->input('instansi_pendidikan'),
            'jurusan' => $request->input('jurusan'),
            'mulai_magang' => $request->input('mulai_magang'),
            'selesai_magang' => $request->input('selesai_magang'),
            'nama_anggota1' => $request->input('nama_anggota1'),
            'no_induk1' => $request->input('no_induk1'),
            'email1' => $request->input('email1'),
            'no_telp1' => $request->input('no_telp1'),
            'nama_anggota2' => $request->input('nama_anggota2'),
            'no_induk2' => $request->input('no_induk2'),
            'email2' => $request->input('email2'),
            'no_telp2' => $request->input('no_telp2'),
            'file_pengajuan' => $file,
            'keterangan' => "Menunggu Keputusan Kepala Biro Kepegawaian",
            'teruskan_biro' => 1,
            'teruskan_bagian' => null,
        ]);

        $request->file('file_pengajuan')->storeAs('file_pengajuan', $file);
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $pengajuan = Pengajuan::find($id);
        return view('user/ubah_form_pengajuan', ['pengajuan' => $pengajuan]);
    }

    public function edit_pengajuan_admin($id)
    {
        $pengajuan = Pengajuan::find($id);
        return view('admin/ubah_pengajuan_admin', ['pengajuan' => $pengajuan]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = uniqid() . '_pengajuan_' . $request->input('nama_pengaju') . '.pdf';
        $pengajuan = Pengajuan::find($id);
        $pengajuan->nama_pengaju = e($request->input('nama_pengaju'));
        $pengajuan->no_induk = $request->input('no_induk');
        $pengajuan->no_telp = $request->input('no_telp');
        $pengajuan->asal_surat = e($request->input('instansi_pendidikan'));
        $pengajuan->jurusan = $request->input('jurusan');
        $pengajuan->mulai_magang = $request->input('mulai_magang');
        $pengajuan->selesai_magang = $request->input('selesai_magang');
        $pengajuan->nama_anggota1 = $request->input('nama_anggota1');
        $pengajuan->no_induk1 = $request->input('no_induk1');
        $pengajuan->email1 = $request->input('email1');
        $pengajuan->no_telp1 = $request->input('no_telp1');
        $pengajuan->nama_anggota2 = $request->input('nama_anggota2');
        $pengajuan->no_induk2 = $request->input('no_induk2');
        $pengajuan->email2 = $request->input('email2');
        $pengajuan->no_telp2 = $request->input('no_telp2');
        $pengajuan->jumlah_anggota = count([$pengajuan->nama_pengaju])+count([$pengajuan->nama_anggota1])+count([$pengajuan->nama_anggota2]);
        $filename = 'C:/xampp/htdocs/simagang/storage/app/file_pengajuan/' . $pengajuan->file_pengajuan;
        $pengajuan->delete();
        unlink($filename);
        $pengajuan->file_pengajuan = $file;
        $request->file('file_pengajuan')->storeAs('file_pengajuan', $file);

        $pengajuan->save();

        return redirect()->route('pengajuan.index');
    }

    public function update_admin(Request $request, $id)
    {

        $pengajuan = Pengajuan::find($id);
        $pengajuan->no_surat = $request->input('no_surat');
        $pengajuan->tgl_surat = $request->input('tgl_surat');
        $pengajuan->perihal = $request->input('perihal');
        if ($request->input('teruskan_biro') == "Biro Kepegawaian") {
            $pengajuan->teruskan_biro = 1;
        } else if ($request->input('teruskan_biro') == "Biro Hubungan Masayarakat") {
            $pengajuan->teruskan_biro = 2;
        } else if ($request->input('teruskan_biro') == "Biro Perencanaan") {
            $pengajuan->teruskan_biro = 3;
        } else if ($request->input('teruskan_biro') == "Biro Keuangan") {
            $pengajuan->teruskan_biro = 4;
        } else if ($request->input('teruskan_biro') == "Biro Umum") {
            $pengajuan->teruskan_biro = 5;
        }
        if($pengajuan->status == 2||3||4){
            $pengajuan->teruskan_bagian = (int) $request->input('bagian');
        }
        else{
            $pengajuan->teruskan_bagian == null;
        }
        $pengajuan->tgl_masuk = $request->input('tgl_masuk');

        $pengajuan->save();
        return redirect()->route('list_pengajuan_admin');
    }

    public function tolak_admin($id)
    {
        $pengajuan = Pengajuan::find($id);
        $pengajuan->status = 0;
        $pengajuan->keterangan = "Ditolak Magang";
        $pengajuan->teruskan_biro = 1;
        $pengajuan->teruskan_bagian = null;
        $pengajuan->save();

        return redirect()->route('list_pengajuan_admin');
    }

    public function terima_ropeg_admin($id)
    {
        $pengajuan = Pengajuan::find($id);
        $pengajuan->status = 2;
        $pengajuan->keterangan = "Menunggu Keputusan Kepala Biro Terkait";
        $pengajuan->save();

        return redirect()->route('list_pengajuan_admin');
    }

    public function teruskan_kabiro($id)
    {
        $pengajuan = Pengajuan::find($id);
        $pengajuan->status = 3;
        $pengajuan->keterangan = "Menunggu Keputusan Kepala Bagian";
        $pengajuan->save();

        return redirect()->route('list_pengajuan_admin');
    }

    public function terima_magang_kabag($id)
    {
        $pengajuan = Pengajuan::find($id);
        $pengajuan->status = 4;
        $pengajuan->keterangan = "Diterima Magang";
        $pengajuan->save();
            Magang::create([
                'id_pengajuan' => $pengajuan->id,
                'id_bagian' => $pengajuan->teruskan_bagian,
                'status_magang' => 1,
            ]);
            return redirect()->route('list_pengajuan_admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function openFile($id)
    {
        $pengajuan = Pengajuan::find($id);
        $filename = 'C:/xampp/htdocs/simagang/storage/app/file_pengajuan/' . $pengajuan->file_pengajuan;
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        readfile($filename);
    }

    public function sudahPengajuan()
    {
        return view('error/sudah_pengajuan');
    }


    public function errorDiproses()
    {
        return view('error/error_pengajuan_diproses');
    }
}
