<?php

namespace App\Http\Controllers;

use App\Magang;
use App\Pengajuan;
use Illuminate\Http\Request;

class MagangController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengajuan = Pengajuan::where(['id_pengguna' => auth()->user()->id])->first();
        $magang = Magang::with(['magang_pengajuan', 'magang_nilai'])->where(['id_pengajuan' => $pengajuan->id])->first();

        if ($pengajuan['status'] == 4)
        {
            return view('user/data_magang', ['magang' => $magang]);
        }
        else
        {
            echo "Status anda belum diterima sebagai siswa magang BKN";
        }
    }

    public function index_admin()
    {
        $magang = Magang::with(['magang_pengajuan', 'magang_nilai'])->where(['id_bagian' => auth()->user()->id_bagian])->get();
        return view('admin/list_magang_admin', ['magang' => $magang]);
    }

    public function selesaikan_magang($id)
    {
        $magang = Magang::find($id);
        $magang->status_magang = 2;
        $magang->save();

        return redirect()->route('list_magang_admin');
    }

    public function error_belum_diterima_magang()
    {
        return view('error/error_belum_magang');
    }

    public function error_sudah_diterima_magang()
    {
        return view('error/error_diterima');
    }

    public function errorPenolakan()
    {
        return view('error/error_ditolak');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
