<?php

namespace App\Http\Controllers;

use App\Pengajuan;
use App\Magang;
use App\Logbook;
use Illuminate\Http\Request;

class IsiLogbookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengajuan = Pengajuan::where(['id_pengguna' => auth()->user()->id])->first();
        $magang = Magang::with(['magang_pengajuan'])->where(['id_pengajuan' => $pengajuan->id])->first();
        $logbook = Logbook::with(['logbook_magang'])->where(['id_magang' => $magang->id])->get();
        if ($pengajuan->status == 4)
        {
            return view('user/list_logbook_magang', ['logbook' => $logbook]);
        }
        else
        {
            return redirect()->route('error_status');
        }
    }

    public function index_admin()
    {

        $magang = Magang::with(['magang_bagian'])->where(['id_bagian' => auth()->user()->id_bagian])->first();
        $logbook = Logbook::with(['logbook_magang.magang_bagian.bagian_pengguna',
        'logbook_magang.magang_pengajuan'])->where(['id_magang' => $magang->id])->get();
        return view('admin/list_logbook_admin', ['logbook' => $logbook]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user/tambah_logbook_magang');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pengajuan = Pengajuan::with(['pengaju'])->where(['id_pengguna' => auth()->user()->id])->first();

        $magang = Magang::with(['magang_pengajuan'])->where(['id_pengajuan' => $pengajuan->id])->first();
        Logbook::create([
            'id_magang' => $magang->id,
            'tanggal' => $request->input('tanggal'),
            'lokasi' => $request->input('lokasi'),
            'keterangan' => $request->input('keterangan'),
        ]);

        return redirect()->route('logbook.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $logbook = Logbook::find($id);
        return view('user/ubah_logbook_magang', ['logbook' => $logbook]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $logbook = Logbook::find($id);
        $logbook->tanggal = $request->input('tanggal');
        $logbook->lokasi = $request->input('lokasi');
        $logbook->keterangan = $request->input('keterangan');
        $logbook->save();

        return redirect()->route('logbook.index');
    }

    public function acc_logbook($id)
    {
        $logbook = Logbook::find($id);
        $logbook->status = 2;
        $logbook->save();
        return redirect()->route('list_logbook_admin');
    }
    public function tolak_logbook($id)
    {
        $logbook = Logbook::find($id);
        $logbook->status = 0;
        $logbook->save();
        return redirect()->route('list_logbook_admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
