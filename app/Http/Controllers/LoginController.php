<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {

        if (Auth::attempt(
            [
                'email' => $request->email,
                'password' => $request->password
            ]
        )) {
            $user = User::where('email', $request->email)->first();

            // dd($user);
            if ($user->is_admin()) {
                // echo "You are an admin!";
                return redirect()->route('admin_dashboard');
            } else {
                return redirect()->route('home');
            }
        } else {
            return redirect()->back();
        }
    }

    public function logout(Request $request){
        $this->performLogout($request);
        return redirect()->route('login');
    }
}
