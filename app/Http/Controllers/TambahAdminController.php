<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class TambahAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('admin/list_pengguna', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = User::find($id);

        return view('admin/ubah_status_pengguna', ['status' => $status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = User::find($id);
        if ($request->input('is_admin') == "Admin") {
            $status->is_admin = 1;
            if ($request->input('id_biro') == "Biro Kepegawaian") {
                $status->id_biro = 1;
            } else if ($request->input('id_biro') == "Biro Hubungan Masyarakat") {
                $status->id_biro = 2;
            } else if ($request->input('id_biro') == "Biro Perencanaan") {
                $status->id_biro = 3;
            } else if ($request->input('id_biro') == "Biro Keuangan") {
                $status->id_biro = 4;
            } else if ($request->input('id_biro') == "Biro Umum") {
                $status->id_biro = 5;
            } else {
                $status->id_biro = 1;
            }

            if ($request->input('id_bagian') == "Ropeg 1") {
                $status->id_biro = 1;
                $status->id_bagian = 1;
            } else if ($request->input('id_bagian') == "Ropeg 2") {
                $status->id_biro = 1;
                $status->id_bagian = 2;
            } else if ($request->input('id_bagian') == "Ropeg 3") {
                $status->id_biro = 1;
                $status->id_bagian = 3;
            } else if ($request->input('id_bagian') == "Ropeg 4") {
                $status->id_biro = 1;
                $status->id_bagian = 4;
            } else if ($request->input('id_bagian') == "Ropeg 5") {
                $status->id_biro = 1;
                $status->id_bagian = 5;
            } else if ($request->input('id_bagian') == "Humas 1") {
                $status->id_biro = 2;
                $status->id_bagian = 6;
            } else if ($request->input('id_bagian') == "Humas 2") {
                $status->id_biro = 2;
                $status->id_bagian = 7;
            } else if ($request->input('id_bagian') == "Humas 3") {
                $status->id_biro = 2;
                $status->id_bagian = 8;
            } else if ($request->input('id_bagian') == "Humas 4") {
                $status->id_biro = 2;
                $status->id_bagian = 9;
            } else if ($request->input('id_bagian') == "Humas 5") {
                $status->id_biro = 2;
                $status->id_bagian = 10;
            } else if ($request->input('id_bagian') == "Perencanaan 1") {
                $status->id_biro = 3;
                $status->id_bagian = 11;
            } else if ($request->input('id_bagian') == "Perencanaan 2") {
                $status->id_biro = 3;
                $status->id_bagian = 12;
            } else if ($request->input('id_bagian') == "Perencanaan 3") {
                $status->id_biro = 3;
                $status->id_bagian = 13;
            } else if ($request->input('id_bagian') == "Perencanaan 4") {
                $status->id_biro = 3;
                $status->id_bagian = 14;
            } else if ($request->input('id_bagian') == "Perencanaan 5") {
                $status->id_biro = 3;
                $status->id_bagian = 15;
            } else if ($request->input('id_bagian') == "Keuangan 1") {
                $status->id_biro = 4;
                $status->id_bagian = 16;
            } else if ($request->input('id_bagian') == "Keuangan 2") {
                $status->id_biro = 4;
                $status->id_bagian = 17;
            } else if ($request->input('id_bagian') == "Keuangan 3") {
                $status->id_biro = 4;
                $status->id_bagian = 18;
            } else if ($request->input('id_bagian') == "Keuangan 4") {
                $status->id_biro = 4;
                $status->id_bagian = 19;
            } else if ($request->input('id_bagian') == "Keuangan 5") {
                $status->id_biro = 4;
                $status->id_bagian = 20;
            } else if ($request->input('id_bagian') == "Umum 1") {
                $status->id_biro = 5;
                $status->id_bagian = 21;
            } else if ($request->input('id_bagian') == "Umum 2") {
                $status->id_biro = 5;
                $status->id_bagian = 22;
            } else if ($request->input('id_bagian') == "Umum 3") {
                $status->id_biro = 5;
                $status->id_bagian = 23;
            } else if ($request->input('id_bagian') == "Umum 4") {
                $status->id_biro = 5;
                $status->id_bagian = 24;
            } else if ($request->input('id_bagian') == "Umum 5") {
                $status->id_biro = 5;
                $status->id_bagian = 25;
            } else {
                $status->id_bagian = NULL;
            }


        } else if ($request->input('is_admin') == "Pengguna") {
            $status->is_admin = NULL;
            $status->id_biro = NULL;
            $status->id_bagian = NULL;
        }
        $status->save();
        return redirect()->route('tambahadmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
