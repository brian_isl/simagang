<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logbook extends Model
{
    protected $fillable =
    [
        'id_magang', 'id_pengguna', 'tanggal', 'lokasi', 'keterangan', 'status', 'id_biro'
    ];

    protected $hidden =
    [
        'created_at', 'updated_at'
    ];

    protected $cast =
    [

    ];

    public function logbook_magang()
    {
        return $this->belongsTo('App\Magang', 'id_magang', 'id');
    }


}
