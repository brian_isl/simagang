<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biro extends Model
{
    protected $guarded = ['nama_biro'];

    public function biro(){
        return $this->hasMany('App\Pengajuan', 'teruskan_biro', 'id');
    }

    // public function biro_magang(){
    //     return $this->hasMany('App\Magang', 'id_biro', 'id');
    // }

    public function biro_logbook(){
        return $this->hasMany('App\Logbook', 'id_biro', 'id');
    }

    public function biro_bagian(){
        return $this->hasMany('App\Bagian', 'id_biro', 'id');
    }

    public function biro_pengguna(){
        return $this->hasMany('App\User', 'id_biro', 'id');
    }

}
