<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdPengajuanColumnToMagangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('magangs', function (Blueprint $table) {
            $table->unsignedBigInteger('id_pengajuan')->after('id_pengguna')->nullable();

            $table->foreign('id_pengajuan')->references('id')->on('pengajuans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magangs', function (Blueprint $table) {
            //
        });
    }
}
