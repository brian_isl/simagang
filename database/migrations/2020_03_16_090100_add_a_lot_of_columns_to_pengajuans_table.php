<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddALotOfColumnsToPengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuans', function (Blueprint $table) {
            $table->string('email')->after('nama_pengaju');
            $table->string('no_telp')->after('email');
            $table->integer('durasi_magang')->after('no_telp');
            $table->string('nama_anggota1')->after('durasi_magang')->nullable();
            $table->string('email1')->after('nama_anggota1')->nullable();
            $table->string('no_telp1')->after('email1')->nullable();
            $table->string('nama_anggota2')->after('no_telp1')->nullable();
            $table->string('email2')->after('nama_anggota2')->nullable();
            $table->string('no_telp2')->after('email2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengajuans', function (Blueprint $table) {
            //
        });
    }
}
