<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNilaiKeaktifanAndNilaiKehadiranAndNilaiTugasColumnToMagangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('magangs', function (Blueprint $table) {
            $table->integer('nilai_kehadiran')->after('no_telp2')->nullable();
            $table->integer('nilai_keaktifan')->after('nilai_kehadiran')->nullable();
            $table->integer('nilai_tugas')->after('nilai_keaktifan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magangs', function (Blueprint $table) {
            //
        });
    }
}
