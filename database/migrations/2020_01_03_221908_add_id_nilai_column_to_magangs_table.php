<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdNilaiColumnToMagangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('magangs', function (Blueprint $table) {
            $table->unsignedBigInteger('id_nilai')->after('id')->default(10);

            $table->foreign('id_nilai')->references('id')->on('nilais')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('magangs', function (Blueprint $table) {
            //
        });
    }
}
