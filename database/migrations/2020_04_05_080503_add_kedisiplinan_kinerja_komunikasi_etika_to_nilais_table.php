<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKedisiplinanKinerjaKomunikasiEtikaToNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nilais', function (Blueprint $table) {
            $table->integer('kedisiplinan')->after('id')->nullable();
            $table->integer('kinerja')->after('kedisiplinan')->nullable();
            $table->integer('komunikasi')->after('kinerja')->nullable();
            $table->integer('etika')->after('komunikasi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nilais', function (Blueprint $table) {
            //
        });
    }
}
