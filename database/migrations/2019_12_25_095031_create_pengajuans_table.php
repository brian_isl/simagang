<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_pengguna');
            $table->string('nama_pengaju');
            $table->string('no_induk');
            $table->string('email');
            $table->string('no_telp');
            $table->date('mulai_magang');
            $table->date('selesai_magang');
            $table->string('nama_anggota1')->nullable();
            $table->string('no_induk1')->nullable();
            $table->string('email1')->nullable();
            $table->string('no_telp1')->nullable();
            $table->string('nama_anggota2')->nullable();
            $table->string('no_induk2')->nullable();
            $table->string('email2')->nullable();
            $table->string('no_telp2')->nullable();
            $table->string('asal_surat');
            $table->string('jurusan');
            $table->string('no_surat')->nullable();
            $table->date('tgl_surat')->nullable();
            $table->string('perihal')->nullable();
            $table->unsignedBigInteger('teruskan_biro');
            $table->unsignedBigInteger('teruskan_bagian')->nullable();
            $table->date('tgl_masuk')->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->string('file_pengajuan');
            $table->timestamps();

            $table->foreign('id_pengguna')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuans');
    }
}
