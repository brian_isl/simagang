<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <meta content="{{ csrf_token() }}" name="csrf-token">
    <title>SiMagang - Badan Kepegawaian Negara</title><!-- Custom fonts for this theme -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <link href="{{asset('/freelancer/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
        type="text/css"><!-- Theme CSS -->
    <link href="{{asset('/freelancer/css/freelancer.min.css')}}" rel="stylesheet">
</head>

<body>
    <div id="app">
        @yield('topbar')
        @yield('content')

    </div>
    @yield('bottombar')


    <!-- Bootstrap core JavaScript -->
    <script src="{{('/freelancer/vendor/jquery/jquery.min.js')}}"></script>
    {{-- <script src="{{('/freelancer/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script> --}}

    <!-- Plugin JavaScript -->
    <script src="{{('/freelancer/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Contact Form JavaScript -->
    <script src="{{('/freelancer/js/jqBootstrapValidation.js')}}"></script>
    <script src="{{('/freelancer/js/contact_me.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{('/freelancer/js/freelancer.min.js')}}"></script>
</body>

</html>
