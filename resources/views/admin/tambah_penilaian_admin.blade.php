@extends('admin.master_admin')
@extends('admin.komponen.sidebar_admin')
@extends('admin.komponen.topbar_admin')

@section('konten_admin')

<form action="{{route('simpan_penilaian', $nilai->id)}}" method="POST">
    <div class="container-fluid">
        @csrf
        <div class="form-group">
            <p>Nilai Kedisiplinan: </p> <input type="number" name="kedisiplinan" class="form-control form-control-user"
                placeholder="Nilai Kedisiplinan" required>
        </div>
        <div class="form-group">
            <p>Nilai Kinerja: </p> <input type="number" name="kinerja" class="form-control form-control-user"
                placeholder="Nilai Kinerja" required>
        </div>
        <div class="form-group">
            <p>Nilai Komunikasi: </p> <input type="number" name="komunikasi" class="form-control form-control-user"
                placeholder="Nilai Komunikasi" required>
        </div>
        <div class="form-group">
            <p>Nilai Etika: </p> <input type="number" name="etika" class="form-control form-control-user"
                placeholder="Nilai Etika" required>
        </div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            Tambah Nilai
        </button>
    </div>
</form>

@endsection
