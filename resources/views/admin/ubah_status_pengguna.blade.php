@extends('admin.master_admin')
@extends('admin.komponen.sidebar_admin')
@extends('admin.komponen.topbar_admin')

@section('konten_admin')

<form action="{{route('tambahadmin.update', $status['id'])}}" method="POST">

    <div class="container-fluid">
        @csrf
        @method('PUT')
        <div class="form-group">
            <p>Nama: </p>
            <input type="text" name="nama" class="form-control form-control-user"
                id="nama" placeholder="Nama" value="{{$status->name}}" readonly>
        </div>
        <div class="form-group">
            <p>Status: </p>
        <select name="is_admin" class="form-control form-control-user">
                <option value="Admin">Admin</option>
                <option value="Pengguna">Pengguna</option>
            </select>
        </div>
        <div class="form-group">
            <p>Biro: </p>
            <select name="id_biro" class="form-control form-control-user">
                <option value="-">-</option>
                <option value="Biro Kepegawaian">Biro Kepegawaian</option>
                <option value="Biro Hubungan Masyarakat">Biro Hubungan Masyarakat</option>
                <option value="Biro Perencanaan">Biro Perencanaan</option>
                <option value="Biro Keuangan">Biro Keuangan</option>
                <option value="Biro Umum">Biro Umum</option>

            </select>
        </div>
        <div class="form-group">
            <p>Biro: </p>
            <select name="id_bagian" class="form-control form-control-user">
                <option value="-">-</option>
                <option value="Ropeg 1">Ropeg 1</option>
                <option value="Ropeg 2">Ropeg 2</option>
                <option value="Ropeg 3">Ropeg 3</option>
                <option value="Ropeg 4">Ropeg 4</option>
                <option value="Ropeg 5">Ropeg 5</option>
                <option value="Humas 1">Humas 1</option>
                <option value="Humas 2">Humas 2</option>
                <option value="Humas 3">Humas 3</option>
                <option value="Humas 4">Humas 4</option>
                <option value="Humas 5">Humas 5</option>
                <option value="Perencanaan 1">Perencanaan 1</option>
                <option value="Perencanaan 2">Perencanaan 2</option>
                <option value="Perencanaan 3">Perencanaan 3</option>
                <option value="Perencanaan 4">Perencanaan 4</option>
                <option value="Perencanaan 5">Perencanaan 5</option>
                <option value="Keuangan 1">Keuangan 1</option>
                <option value="Keuangan 2">Keuangan 2</option>
                <option value="Keuangan 3">Keuangan 3</option>
                <option value="Keuangan 4">Keuangan 4</option>
                <option value="Keuangan 5">Keuangan 5</option>
                <option value="Umum 1">Umum 1</option>
                <option value="Umum 2">Umum 2</option>
                <option value="Umum 3">Umum 3</option>
                <option value="Umum 4">Umum 4</option>
                <option value="Umum 5">Umum 5</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            Ubah Status
        </button>
    </div>
</form>

@endsection
