@extends('admin.master_admin')
@extends('admin.komponen.sidebar_admin')
@extends('admin.komponen.topbar_admin')

@section('konten_admin')

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Daftar Pengguna</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Pengguna</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div class="dataTables_wrapper dt-bootstrap4" id="dataTable_wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <table aria-describedby="dataTable_info" cellspacing="0"
                                class="table table-bordered dataTable" id="dataTable" role="grid" style="width: 100%;"
                                width="100%">
                                <thead>
                                    <tr role="row">
                                        <th aria-controls="dataTable"
                                            aria-label="Nama: activate to sort column descending"
                                            aria-sort="ascending" class="sorting_asc" colspan="1" rowspan="1"
                                            style="width: 64px;" tabindex="0">Nama</th>
                                        <th aria-controls="dataTable"
                                            aria-label="E-Mail: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 81px;" tabindex="0">
                                            E-Mail</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Status: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 81px;" tabindex="0">
                                            Status</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Biro: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 31px;" tabindex="0">
                                            Biro</th>
                                            <th aria-controls="dataTable"
                                            aria-label="Biro: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 31px;" tabindex="0">
                                            Bagian</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Ubah Status: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">Ubah Status</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Hapus: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">Hapus</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="1" rowspan="1">Nama</th>
                                        <th colspan="1" rowspan="1">E-Mail</th>
                                        <th colspan="1" rowspan="1">Status</th>
                                        <th colspan="1" rowspan="1">Biro</th>
                                        <th colspan="1" rowspan="1">Bagian</th>
                                        <th colspan="1" rowspan="1">Ubah Status</th>
                                        <th colspan="1" rowspan="1">Hapus</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($user as $user)
                                    <tr class="odd" role="row">
                                        <td class="sorting_1">{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        @if($user->is_admin==1)
                                        <td>Admin</td>
                                        @else
                                        <td>Pengguna</td>
                                        @endif
                                        @if($user->id_biro==1)
                                        <td>Biro Kepegawaian</td>
                                        @elseif($user->id_biro==2)
                                        <td>Biro Hubungan Masyarakat</td>
                                        @elseif($user->id_biro==3)
                                        <td>Biro Perencanaan</td>
                                        @elseif($user->id_biro==4)
                                        <td>Biro Keuangan</td>
                                        @elseif($user->id_biro==5)
                                        <td>Biro Umum</td>
                                        @else
                                        <td>-</td>
                                        @endif
                                        @if($user->id_bagian==1)
                                        <td>Ropeg 1</td>
                                        @elseif($user->id_bagian==2)
                                        <td>Ropeg 2</td>
                                        @elseif($user->id_bagian==3)
                                        <td>Ropeg 3</td>
                                        @elseif($user->id_bagian==4)
                                        <td>Ropeg 4</td>
                                        @elseif($user->id_bagian==5)
                                        <td>Ropeg 5</td>
                                        @elseif($user->id_bagian==6)
                                        <td>Humas 1</td>
                                        @elseif($user->id_bagian==7)
                                        <td>Humas 2</td>
                                        @elseif($user->id_bagian==8)
                                        <td>Humas 3</td>
                                        @elseif($user->id_bagian==9)
                                        <td>Humas 4</td>
                                        @elseif($user->id_bagian==10)
                                        <td>Humas 5</td>
                                        @elseif($user->id_bagian==11)
                                        <td>Perencanaan 1</td>
                                        @elseif($user->id_bagian==12)
                                        <td>Perencanaan 2</td>
                                        @elseif($user->id_bagian==13)
                                        <td>Perencanaan 3</td>
                                        @elseif($user->id_bagian==14)
                                        <td>Perencanaan 4</td>
                                        @elseif($user->id_bagian==15)
                                        <td>Perencanaan 5</td>
                                        @elseif($user->id_bagian==16)
                                        <td>Keuangan 1</td>
                                        @elseif($user->id_bagian==17)
                                        <td>Keuangan 2</td>
                                        @elseif($user->id_bagian==18)
                                        <td>Keuangan 3</td>
                                        @elseif($user->id_bagian==19)
                                        <td>Keuangan 4</td>
                                        @elseif($user->id_bagian==20)
                                        <td>Keuangan 5</td>
                                        @elseif($user->id_bagian==21)
                                        <td>Umum 1</td>
                                        @elseif($user->id_bagian==22)
                                        <td>Umum 2</td>
                                        @elseif($user->id_bagian==23)
                                        <td>Umum 3</td>
                                        @elseif($user->id_bagian==24)
                                        <td>Umum 4</td>
                                        @elseif($user->id_bagian==25)
                                        <td>Umum 5</td>
                                        @else
                                        <td>-</td>
                                        @endif
                                        <td>
                                            <a href="{{route('tambahadmin.edit', $user['id'])}}">
                                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                                    Ubah
                                                </button>
                                            </a>
                                        </td>
                                        <td>
                                            <form action="#{{--route('pengajuan.destroy', [$pengajuan['id']])--}}"
                                                method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-user btn-block">
                                                    Hapus
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
