@extends('admin.master_admin')
@extends('admin.komponen.sidebar_admin')
@extends('admin.komponen.topbar_admin')

@section('konten_admin')
<!-- Begin Page Content -->
<form action="{{route ('update_pengajuan_admin', $pengajuan->id)}}" method="post">


    <div class="container-fluid">
        @csrf
        <div class="form-group">
            <p>Nama Pengaju: </p> <input type="text" name="nama_pengaju" class="form-control form-control-user"
                id="nama_pengaju" placeholder="Nama Pengaju" value="{{$pengajuan['nama_pengaju']}}" readonly>
        </div>
        <div class="form-group">
            <p>Asal Surat: </p> <input type="text" name="asal_surat" class="form-control form-control-user"
                id="asal_surat" placeholder="Asal Surat" value="{{$pengajuan['asal_surat']}}" readonly>
        </div>
        <div class="form-group">
            <p>Nomor Surat: </p> <input type="text" name="no_surat" class="form-control form-control-user" id="no_surat"
                placeholder="Nomor Surat" value="{{$pengajuan['no_surat']}}" required>
        </div>
        <div class="form-group">
            <p>Tanggal Surat: </p> <input type="date" name="tgl_surat" class="form-control form-control-user"
                id="tgl_surat" placeholder="Tanggal Surat" value="{{$pengajuan['tgl_surat']}}" required>
        </div>
        <div class="form-group">
            <p>Perihal: </p> <input type="text" name="perihal" class="form-control form-control-user" id="perihal"
                placeholder="Perihal" value="{{$pengajuan['perihal']}}" required>
        </div>
        {{-- <div class="form-group">
                <p>Diteruskan Ke Unit: </p> <input type="number" name="teruskan" class="form-control form-control-user" id="teruskan" placeholder="Diteruskan Ke Unit" value="{{$pengajuan['teruskan']}}">
    </div> --}}
    @if($pengajuan['status']!=2)
    <div class="form-group">
        <p>Diteruskan Ke Biro: </p>
        <select name="teruskan_biro" id="kelulusan" class="form-control form-control-user" required>
            <option value="Biro Kepegawaian">Biro Kepegawaian</option>
            <option value="Biro Hubungan Masyarakat">Biro Hubungan Masyarakat</option>
            <option value="Biro Perencanaan">Biro Perencanaan</option>
            <option value="Biro Keuangan">Biro Keuangan</option>
            <option value="Biro Umum">Biro Umum</option>
        </select>
    </div>
    @else
    <div class="form-group">
        <p>Diteruskan Ke Bagian: </p>
        @if($pengajuan['teruskan_biro']==1)
        <select name="bagian" id="bagian" class="form-control form-control-user" required>
            <option value="1">Ropeg 1</option>
            <option value="2">Ropeg 2</option>
            <option value="3">Ropeg 3</option>
            <option value="4">Ropeg 4</option>
            <option value="5">Ropeg 5</option>
        </select>
        @elseif($pengajuan['teruskan_biro']==2)
        <select name="bagian" id="bagian" class="form-control form-control-user" required>
            <option value="6">Humas 1</option>
            <option value="7">Humas 2</option>
            <option value="8">Humas 3</option>
            <option value="9">Humas 4</option>
            <option value="10">Humas 5</option>
        </select>
        @elseif($pengajuan['teruskan_biro']==3)
        <select name="bagian" id="bagian" class="form-control form-control-user" required>
            <option value="11">Perencanaan 1</option>
            <option value="12">Perencanaan 2</option>
            <option value="23">Perencanaan 3</option>
            <option value="14">Perencanaan 4</option>
            <option value="15">Perencanaan 5</option>
        </select>
        @elseif($pengajuan['teruskan_biro']==4)
        <select name="bagian" id="bagian" class="form-control form-control-user" required>
            <option value="16">Keuangan 1</option>
            <option value="17">Keuangan 2</option>
            <option value="18">Keuangan 3</option>
            <option value="19">Keuangan 4</option>
            <option value="20">Keuangan 5</option>
        </select>
        @elseif($pengajuan['teruskan_biro']==5)
        <select name="bagian" id="bagian" class="form-control form-control-user" required>
            <option value="21">Umum 1</option>
            <option value="22">Umum 2</option>
            <option value="23">Umum 3</option>
            <option value="24">Umum 4</option>
            <option value="25">Umum 5</option>
        </select>
        @endif
    </div>
    @endif
    <div class="form-group">
        <p>Tanggal Masuk Surat: </p> <input type="date" name="tgl_masuk" class="form-control form-control-user"
            id="tgl_masuk" placeholder="Tanggal Masuk Surat" value="{{$pengajuan['tgl_masuk']}}" required>
    </div>
    {{-- <div class="form-group">
        <p>Keterangan: </p> <input type="text" name="keterangan" class="form-control form-control-user" id="keterangan"
            placeholder="Keterangan" value="{{$pengajuan['keterangan']}}" required>
    </div> --}}
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Ubah Data
    </button>
    </div>
</form>
<!-- /.container-fluid -->


<!-- End of Main Content -->
@endsection
