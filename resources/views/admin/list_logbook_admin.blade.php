@extends('admin.master_admin')
@extends('admin.komponen.sidebar_admin')
@extends('admin.komponen.topbar_admin')

@section('konten_admin')

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Daftar Logbook Kegiatan Sehari-hari</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <div class="dataTables_wrapper dt-bootstrap4" id="dataTable_wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <table aria-describedby="dataTable_info" cellspacing="0"
                                class="table table-bordered dataTable" id="dataTable" role="grid" style="width: 100%;"
                                width="100%">
                                <thead>
                                    <tr role="row">
                                        <th aria-controls="dataTable"
                                            aria-label="Nama Ketua: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 56px;" tabindex="0">Nama Ketua</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Tanggal Kegiatan: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 31px;" tabindex="0">
                                            Tanggal Kegiatan</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Lokasi Kegiatan: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 68px;" tabindex="0">
                                            Lokasi Kegiatan</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Keterangan: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Keterangan</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Status: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Status</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Acc: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Acc</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Tolak: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Tolak</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="1" rowspan="1">Nama Ketua</th>
                                        <th colspan="1" rowspan="1">Tanggal Kegiatan</th>
                                        <th colspan="1" rowspan="1">Lokasi Kegiatan</th>
                                        <th colspan="1" rowspan="1">Keterangan</th>
                                        <th colspan="1" rowspan="1">Status</th>
                                        <th colspan="1" rowspan="1">Acc</th>
                                        <th colspan="1" rowspan="1">Tolak</th>

                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($logbook as $logbook)
                                    <tr class="odd" role="row">
                                        <td>{{$logbook->logbook_magang->magang_pengajuan->nama_pengaju}}</td>
                                        <td>{{$logbook->tanggal}}</td>
                                        <td>{{$logbook->lokasi}}</td>
                                        <td>{{$logbook->keterangan}}</td>
                                        @if($logbook->status==1)
                                        <td>Menunggu Verifikasi</td>
                                        @elseif($logbook->status==2)
                                        <td>Diterima</td>
                                        @elseif($logbook->status==0)
                                        <td>Ditolak</td>
                                        @endif
                                        @if($logbook->status==2)
                                        <td></td>
                                        @else
                                        <td>
                                            <form action="{{route('acc_logbook', $logbook['id'])}}" method="post">
                                                @csrf
                                                @method('POST')
                                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                                    Acc
                                                </button>
                                            </form>
                                        </td>
                                        @endif
                                        @if($logbook->status==0)
                                        <td></td>
                                        @else
                                        <td>
                                            <form action="{{route('tolak_logbook', $logbook['id'])}}" method="post">
                                                @csrf
                                                @method('POST')
                                                <button type="submit" class="btn btn-danger btn-user btn-block">
                                                    Tolak
                                                </button>
                                            </form>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
