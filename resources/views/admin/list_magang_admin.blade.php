@extends('admin.master_admin')
@extends('admin.komponen.sidebar_admin')
@extends('admin.komponen.topbar_admin')

@section('konten_admin')

<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Daftar Siswa Magang</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <div class="dataTables_wrapper dt-bootstrap4" id="dataTable_wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <table aria-describedby="dataTable_info" cellspacing="0"
                                class="table table-bordered dataTable" id="dataTable" role="grid" style="width: 100%;"
                                width="100%">
                                <thead>
                                    <tr role="row">
                                        <th aria-controls="dataTable"
                                            aria-label="Instansi Pendidikan: activate to sort column descending"
                                            aria-sort="ascending" class="sorting_asc" colspan="1" rowspan="1"
                                            style="width: 64px;" tabindex="0">Instansi Pendidikan</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Mulai Magang: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 56px;" tabindex="0">Mulai Magang
                                            (Tahun-Hari-Bulan)</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Selesai Magang: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 56px;" tabindex="0">
                                            Selesai Magang (Tahun-Hari-Bulan)</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Nama Ketua: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 56px;" tabindex="0">Nama Ketua</th>
                                        <th aria-controls="dataTable"
                                            aria-label="E-Mail Ketua: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 31px;" tabindex="0">
                                            E-Mail Ketua</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Nomor Telpon Ketua: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 68px;" tabindex="0">
                                            Nomor Telpon Ketua</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Nama Anggota 1: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Nama Anggota 1</th>
                                        <th aria-controls="dataTable"
                                            aria-label="E-Mail Anggota 1: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            E-Mail Anggota 1</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Nomor Telpon Anggota 1: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Nomor Telpon Anggota 1</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Nama Anggota 2: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Nama Anggota 2</th>
                                        <th aria-controls="dataTable"
                                            aria-label="E-Mail Anggota 2: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            E-Mail Anggota 2</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Nomor Telpon Anggota 2: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Nomor Telpon Anggota 2</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Status Magang: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Status Magang</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Nilai Akhir Kelompok: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Nilai Akhir Kelompok</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Beri Penilaian: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Penilaian</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Selesaikan Magang: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Selesaikan Magang</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="1" rowspan="1">Instansi Pendidikan</th>
                                        <th colspan="1" rowspan="1">Mulai Magang (Tahun-Hari-Bulan)</th>
                                        <th colspan="1" rowspan="1">Selesai Magang (Tahun-Hari-Bulan)</th>
                                        <th colspan="1" rowspan="1">Nama Ketua</th>
                                        <th colspan="1" rowspan="1">E-Mail Ketua</th>
                                        <th colspan="1" rowspan="1">Nomor Telpon Ketua</th>
                                        <th colspan="1" rowspan="1">Nama Anggota 1</th>
                                        <th colspan="1" rowspan="1">E-Mail Anggota 1</th>
                                        <th colspan="1" rowspan="1">Nomor Telpon Anggota 1</th>
                                        <th colspan="1" rowspan="1">Nama Anggota 2</th>
                                        <th colspan="1" rowspan="1">E-Mail Anggota 2</th>
                                        <th colspan="1" rowspan="1">Nomor Telpon Anggota 2</th>
                                        <th colspan="1" rowspan="1">Status Magang</th>
                                        <th colspan="1" rowspan="1">Nilai Akhir Kelompok</th>
                                        <th colspan="1" rowspan="1">Penilaian</th>
                                        <th colspan="1" rowspan="1">Selesaikan Magang</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($magang as $magang)
                                    <tr class="odd" role="row">
                                        <td class="sorting_1">{{$magang->magang_pengajuan->asal_surat}}</td>
                                        <td>{{$magang->magang_pengajuan->mulai_magang}}</td>
                                        <td>{{$magang->magang_pengajuan->selesai_magang}}</td>
                                        <td>{{$magang->magang_pengajuan->nama_pengaju}}</td>
                                        <td>{{$magang->magang_pengajuan->email}}</td>
                                        <td>{{$magang->magang_pengajuan->no_telp}}</td>
                                        <td>{{$magang->magang_pengajuan->nama_anggota1}}</td>
                                        <td>{{$magang->magang_pengajuan->email1}}</td>
                                        <td>{{$magang->magang_pengajuan->no_telp1}}</td>
                                        <td>{{$magang->magang_pengajuan->nama_anggota2}}</td>
                                        <td>{{$magang->magang_pengajuan->email2}}</td>
                                        <td>{{$magang->magang_pengajuan->no_telp2}}</td>
                                        @if($magang->status_magang == 1)
                                        <td>Magang</td>
                                        @else
                                        <td>Selesai Magang</td>
                                        @endif
                                        @if ($magang->magang_nilai['nilai_akhir']>85)
                                        <td>A</td>
                                        @elseif($magang->magang_nilai['nilai_akhir']>=70)
                                        <td>B</td>
                                        @elseif($magang->magang_nilai['nilai_akhir']>=55)
                                        <td>C</td>
                                        @elseif($magang->magang_nilai['nilai_akhir']<=54) <td>D</td>
                                            @endif
                                            @if($magang->magang_nilai['id_magang']==null)
                                            <td>
                                                <a href="{{route('tambah_penilaian', $magang['id'])}}">
                                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                                    Beri Penilaian
                                                </button>
                                            </a>
                                            </td>
                                            @else
                                            <td>
                                                <a href="{{route('penilaian.edit', $magang['id'])}}">
                                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                                        Ubah Penilaian
                                                    </button>
                                                </a>
                                            </td>
                                            @endif
                                            @if ($magang->status_magang == 1)
                                            @if ($magang->magang_nilai['id_magang'] != null)
                                            <td>
                                                <form action="{{route('selesaikan_magang', $magang['id'])}}" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                                        Selesaikan Magang
                                                    </button>
                                                </form>
                                            </td>

                                            @else

                                            <td></td>
                                            @endif
                                            @else
                                            <td></td>
                                            @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection
