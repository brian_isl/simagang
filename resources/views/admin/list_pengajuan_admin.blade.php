@extends('admin.master_admin')
@extends('admin.komponen.sidebar_admin')
@extends('admin.komponen.topbar_admin')

@section('konten_admin')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Daftar Pengajuan Magang</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        {{-- <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Pengajuan Magang</h6>
        </div> --}}
        <div class="card-body">
            <div class="table-responsive">
                <div class="dataTables_wrapper dt-bootstrap4" id="dataTable_wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <table aria-describedby="dataTable_info" cellspacing="0"
                                class="table table-bordered dataTable" id="dataTable" role="grid" style="width: 100%;"
                                width="100%">
                                <thead>
                                    <tr role="row">
                                        <th aria-controls="dataTable"
                                            aria-label="Nama Pengaju: activate to sort column descending"
                                            aria-sort="ascending" class="sorting_asc" colspan="1" rowspan="1"
                                            style="width: 64px;" tabindex="0">Nama Pengaju</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Asal Surat: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 81px;" tabindex="0">Asal Surat</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Mulai Magang: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 81px;" tabindex="0">
                                            Mulai Magang (Tahun-Hari-Bulan)</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Selesai Magang: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 81px;" tabindex="0">
                                            Selesai Magang (Tahun-Hari-Bulan)</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Nomor Surat: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 56px;" tabindex="0">Nomor Surat</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Tanggal Surat: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 31px;" tabindex="0">
                                            Tanggal Surat</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Perihal: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 68px;" tabindex="0">Perihal</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Diteruskan Ke Unit: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Diteruskan Ke Unit</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Tanggal Masuk: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            Tanggal Masuk Surat</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Keterangan: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">Keterangan</th>
                                        <th aria-controls="dataTable"
                                            aria-label="File Pengajuan: activate to sort column ascending"
                                            class="sorting" colspan="1" rowspan="1" style="width: 67px;" tabindex="0">
                                            File Pengajuan</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Ubah Data: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">Ubah Data</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Terima: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">Terima</th>
                                        <th aria-controls="dataTable"
                                            aria-label="Tolak: activate to sort column ascending" class="sorting"
                                            colspan="1" rowspan="1" style="width: 67px;" tabindex="0">Tolak</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="1" rowspan="1">Nama Pengaju</th>
                                        <th colspan="1" rowspan="1">Asal Surat</th>
                                        <th colspan="1" rowspan="1">Mulai Magang (Tahun-Hari-Bulan)</th>
                                        <th colspan="1" rowspan="1">Selesai Magang (Tahun-Hari-Bulan)</th>
                                        <th colspan="1" rowspan="1">Nomor Surat</th>
                                        <th colspan="1" rowspan="1">Tanggal Surat</th>
                                        <th colspan="1" rowspan="1">Perihal</th>
                                        <th colspan="1" rowspan="1">Diteruskan Ke Unit</th>
                                        <th colspan="1" rowspan="1">Tanggal Masuk Surat</th>
                                        <th colspan="1" rowspan="1">Keterangan</th>
                                        <th colspan="1" rowspan="1">File Pengajuan</th>
                                        <th colspan="1" rowspan="1">Ubah Data</th>
                                        <th colspan="1" rowspan="1">Terima</th>
                                        <th colspan="1" rowspan="1">Tolak</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach ($pengajuan as $pengajuan)
                                    <tr class="odd" role="row">
                                        <td class="sorting_1">{{$pengajuan->nama_pengaju}}</td>
                                        <td>{{$pengajuan->asal_surat}}</td>
                                        <td>{{$pengajuan->mulai_magang}}</td>
                                        <td>{{$pengajuan->selesai_magang}}</td>
                                        <td>{{$pengajuan->no_surat}}</td>
                                        <td>{{$pengajuan->tgl_surat}}</td>
                                        <td>{{$pengajuan->perihal}}</td>
                                        @if($pengajuan->teruskan_biro==1)
                                        <td>Biro Kepegawaian</td>
                                        @elseif($pengajuan->teruskan_biro==2)
                                        <td>Biro Hubungan Masyarakat</td>
                                        @elseif($pengajuan->teruskan_biro==3)
                                        <td>Biro Perencanaan</td>
                                        @elseif($pengajuan->teruskan_biro==4)
                                        <td>Biro Keuangan</td>
                                        @elseif($pengajuan->teruskan_biro==5)
                                        <td>Biro Umum</td>
                                        @endif
                                        <td>{{$pengajuan->tgl_masuk}}</td>
                                        <td>{{$pengajuan->keterangan}}</td>
                                        <td>
                                            <a href="{{route('open_file', [$pengajuan['id']])}}">
                                                <button type="button" class="btn btn-primary btn-user btn-block">
                                                    Lihat File
                                                </button>
                                            </a>
                                        </td>
                                        @if($pengajuan->status == 3)
                                        <td></td>
                                        @elseif($pengajuan->status == 4)
                                        <td></td>
                                        @else
                                        <td>
                                            <a href="{{route('edit_pengajuan_admin', [$pengajuan['id']])}}">
                                                <button type="button" class="btn btn-primary btn-user btn-block">
                                                    Ubah
                                                </button>
                                            </a>
                                        </td>
                                        @endif
                                        <td>
                                            @if($pengajuan->status==1||$pengajuan->status==0)
                                            <form action="{{route('terima_pengajuan', [$pengajuan['id']])}}"
                                                method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                                    Acc
                                                </button>
                                            </form>
                                            @elseif($pengajuan->status==2)
                                            @if ($pengajuan->teruskan_biro != auth()->user()->id_biro)
                                        </td>
                                        @elseif ($pengajuan->teruskan_biro == auth()->user()->id_biro)
                                        <form action="{{route('teruskan_kabiro', [$pengajuan['id']])}}" method="post">
                                            @csrf
                                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                                Teruskan
                                            </button>
                                        </form>
                                        @endif

                                        @elseif ($pengajuan->status==3)
                                        @if (auth()->user()->id_biro == $pengajuan['teruskan_biro'])
                                        @if (auth()->user()->id_bagian != null)

                                        <form action="{{route('terima_magang', [$pengajuan['id']])}}" method="post">
                                            @csrf
                                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                                Terima
                                            </button>
                                        </form>
                                        @endif
                                        @else
                                        @endif


                                        @endif
                                        </td>
                                        <td>

                                            @if($pengajuan->status==0)

                                            @elseif($pengajuan->status==4)

                                            @elseif($pengajuan->status==2)

                                            @else
                                            @if(auth()->user()->id_biro==1)
                                            <form action="{{route('tolak_pengajuan', [$pengajuan['id']])}}"
                                                method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-danger btn-user btn-block">
                                                    Tolak
                                                </button>
                                            </form>
                                            @elseif(auth()->user()->id_bagian!=null)
                                            <form action="{{route('tolak_pengajuan', [$pengajuan['id']])}}"
                                                method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-danger btn-user btn-block">
                                                    Tolak
                                                </button>
                                            </form>
                                            @else
                                            @endif

                                            @endif
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<!-- End of Main Content -->
@endsection
