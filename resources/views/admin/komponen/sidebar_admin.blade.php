 @section('sidebar_admin')


 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

     <!-- Sidebar - Brand -->
     <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('admin_dashboard')}}">
         <div class="sidebar-brand-icon">
             <i class="fas fa-user"></i>
         </div>
         <div class="sidebar-brand-text mx-3">SiMagang</div>
     </a>

     <!-- Divider -->
     <hr class="sidebar-divider my-0">

     <!-- Nav Item - Dashboard -->
     <li class="nav-item">
         <a class="nav-link" href="{{route('admin_dashboard')}}">
             <i class="fas fa-fw fa-tachometer-alt"></i>
             <span>Beranda</span></a>
     </li>

     <!-- Divider -->
     <hr class="sidebar-divider">

     <!-- Heading -->
     <div class="sidebar-heading">
         Daftar
     </div>

     <!-- Nav Item - Tables -->
     <li class="nav-item">
         <a class="nav-link" href="{{route('list_pengajuan_admin')}}">
             <i class="fas fa-fw fa-file"></i>
             <span>Pengajuan</span></a>
     </li>
     @if (Auth::user()->id_bagian == null)

     @else
     <!-- Nav Item - Tables -->
     <li class="nav-item">
         <a class="nav-link" href="{{route('list_magang_admin')}}">
             <i class="fas fa-fw fa-users"></i>
             <span>Data Magang</span></a>
     </li>

     <!-- Nav Item - Tables -->
     <li class="nav-item">
         <a class="nav-link" href="{{route('list_logbook_admin')}}">
             <i class="fas fa-fw fa-book"></i>
             <span>Data Logbook</span></a>
     </li>
     @endif
     @if (Auth::user()->id_biro == 1 && Auth::user()->id_bagian == null)
     <li class="nav-item">
        <a class="nav-link" href="{{route('tambahadmin.index')}}">
            <i class="fas fa-fw fa-user"></i>
            <span>Daftar Pengguna</span></a>
    </li>
     @if (Auth::user()->id_biro == null)
     <!-- Nav Item - Pengguna -->

     @endif
     @else

     @endif



     <!-- Sidebar Toggler (Sidebar) -->
     <div class="text-center d-none d-md-inline">
         <button class="rounded-circle border-0" id="sidebarToggle"></button>
     </div>

 </ul>
 <!-- End of Sidebar -->
 @endsection
