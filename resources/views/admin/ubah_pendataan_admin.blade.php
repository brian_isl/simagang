@extends('admin.master_admin')
@extends('admin.komponen.sidebar_admin')
@extends('admin.komponen.topbar_admin')

@section('konten_admin')

<form action="{{route ('update_pendataan_admin', $magang->id)}}" method="post">


    <div class="container-fluid">
        @csrf
        @if($magang['jumlah_anggota']==3)
        <div class="form-group">
            <p>Instansi Pendidikan: </p> <input type="text" name="instansi_pendidikan"
                class="form-control form-control-user" placeholder="Instansi Pendidikan"
                value="{{$magang['instansi_pendidikan']}}" readonly>
        </div>
        <div class="form-group">
            <p>Durasi Magang: </p> <input min="1" type="number" name="durasi_magang"
                class="form-control form-control-user" placeholder="Durasi Magang" value="{{$magang['durasi_magang']}}">
        </div>
        <div class="form-group">
            <p>Jumlah Anggota: </p> <input type="text" name="jumlah_anggota" class="form-control form-control-user"
                placeholder="Jumlah Anggota" value="{{$magang['jumlah_anggota']}}" readonly>
        </div>
        <div class="form-group">
            <p>Nama Ketua: </p> <input type="text" name="nama_ketua" class="form-control form-control-user"
                placeholder="Nama Ketua" value="{{$magang['nama_ketua']}}">
        </div>
        <div class="form-group">
            <p>E-Mail Ketua: </p> <input type="text" name="email" class="form-control form-control-user"
                placeholder="E-Mail" value="{{$magang['email']}}">
        </div>
        <div class="form-group">
            <p>Nomor Telpon Ketua: </p> <input type="text" name="no_telp" class="form-control form-control-user"
                id="no_surat" placeholder="Nomor Telpon" value="{{$magang['no_telp']}}">
        </div>
        <div class="form-group">
            <p>Nama Anggota 1: </p> <input type="text" name="nama_anggota1" class="form-control form-control-user"
                placeholder="Nama Anggota 1" value="{{$magang['nama_anggota1']}}">
        </div>
        <div class="form-group">
            <p>E-Mail Anggota 1: </p> <input type="text" name="email1" class="form-control form-control-user"
                placeholder="E-Mail" value="{{$magang['email1']}}">
        </div>
        <div class="form-group">
            <p>Nomor Telpon Anggota 1: </p> <input type="text" name="no_telp1" class="form-control form-control-user"
                id="no_surat" placeholder="Nomor Telpon" value="{{$magang['no_telp1']}}">
        </div>
        <div class="form-group">
            <p>Nama Anggota 2: </p> <input type="text" name="nama_anggota2" class="form-control form-control-user"
                placeholder="Nama Anggota 2" value="{{$magang['nama_anggota2']}}">
        </div>
        <div class="form-group">
            <p>E-Mail Anggota 2: </p> <input type="text" name="email2" class="form-control form-control-user"
                placeholder="E-Mail" value="{{$magang['email2']}}">
        </div>
        <div class="form-group">
            <p>Nomor Telpon Anggota 2: </p> <input type="text" name="no_telp2" class="form-control form-control-user"
                id="no_surat" placeholder="Nomor Telpon" value="{{$magang['no_telp2']}}">
        </div>

        @elseif($magang['jumlah_anggota']==2)
        <div class="form-group">
            <p>Instansi Pendidikan: </p> <input type="text" name="instansi_pendidikan"
                class="form-control form-control-user" placeholder="Instansi Pendidikan"
                value="{{$magang['instansi_pendidikan']}}" readonly>
        </div>
        <div class="form-group">
            <p>Durasi Magang: </p> <input min="1" type="number" name="durasi_magang"
                class="form-control form-control-user" placeholder="Durasi Magang" value="{{$magang['durasi_magang']}}">
        </div>
        <div class="form-group">
            <p>Jumlah Anggota: </p> <input type="text" name="jumlah_anggota" class="form-control form-control-user"
                placeholder="Jumlah Anggota" value="{{$magang['jumlah_anggota']}}" readonly>
        </div>
        <div class="form-group">
            <p>Nama Ketua: </p> <input type="text" name="nama_ketua" class="form-control form-control-user"
                placeholder="Nama Ketua" value="{{$magang['nama_ketua']}}">
        </div>
        <div class="form-group">
            <p>E-Mail Ketua: </p> <input type="text" name="email" class="form-control form-control-user"
                placeholder="E-Mail" value="{{$magang['email']}}">
        </div>
        <div class="form-group">
            <p>Nomor Telpon Ketua: </p> <input type="text" name="no_telp" class="form-control form-control-user"
                id="no_surat" placeholder="Nomor Telpon" value="{{$magang['no_telp']}}">
        </div>
        <div class="form-group">
            <p>Nama Anggota 1: </p> <input type="text" name="nama_anggota1" class="form-control form-control-user"
                placeholder="Nama Anggota 1" value="{{$magang['nama_anggota1']}}">
        </div>
        <div class="form-group">
            <p>E-Mail Anggota 1: </p> <input type="text" name="email1" class="form-control form-control-user"
                placeholder="E-Mail" value="{{$magang['email1']}}">
        </div>
        <div class="form-group">
            <p>Nomor Telpon Anggota 1: </p> <input type="text" name="no_telp1" class="form-control form-control-user"
                id="no_surat" placeholder="Nomor Telpon" value="{{$magang['no_telp1']}}">
        </div>

        @elseif($magang['jumlah_anggota']==1)
        <div class="form-group">
            <p>Instansi Pendidikan: </p> <input type="text" name="instansi_pendidikan"
                class="form-control form-control-user" placeholder="Instansi Pendidikan"
                value="{{$magang['instansi_pendidikan']}}" readonly>
        </div>
        <div class="form-group">
            <p>Durasi Magang: </p> <input min="1" type="number" name="durasi_magang"
                class="form-control form-control-user" placeholder="Durasi Magang" value="{{$magang['durasi_magang']}}">
        </div>
        <div class="form-group">
            <p>Jumlah Anggota: </p> <input type="text" name="jumlah_anggota" class="form-control form-control-user"
                placeholder="Jumlah Anggota" value="{{$magang['jumlah_anggota']}}" readonly>
        </div>
        <div class="form-group">
            <p>Nama Ketua: </p> <input type="text" name="nama_ketua" class="form-control form-control-user"
                placeholder="Nama Ketua" value="{{$magang['nama_ketua']}}">
        </div>
        <div class="form-group">
            <p>E-Mail Ketua: </p> <input type="text" name="email" class="form-control form-control-user"
                placeholder="E-Mail" value="{{$magang['email']}}">
        </div>
        <div class="form-group">
            <p>Nomor Telpon Ketua: </p> <input type="text" name="no_telp" class="form-control form-control-user"
                id="no_surat" placeholder="Nomor Telpon" value="{{$magang['no_telp']}}">
        </div>
        @endif
        <button type="submit" class="btn btn-primary btn-user btn-block">
            Ubah Data
        </button>
    </div>
</form>



@endsection
