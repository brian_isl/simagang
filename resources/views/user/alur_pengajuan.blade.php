@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')
<!-- Portfolio Section -->
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        {{-- <section class="page-section portfolio" id="alurpengajuan"> --}}
        <div class="container">
            <!-- Portfolio Section Heading -->
            <h2 class="page-section-heading text-center text-white mb-0">Alur Pengajuan Magang</h2><!-- Icon Divider -->
            <div class="divider-custom divider-light">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon">
                    <i class="fas fa-star"></i>
                </div>
                <div class="divider-custom-line"></div>
            </div>
        </div>
        <div class="divider-custom">
            <div class="divider-custom-icon">
                <p class="masthead-subheading font-weight-light mb-0 text-center text-white">1</p>
                <p class="masthead-subheading font-weight-light mb-0 text-white">Melakukan registrasi di <i>website</i>
                    SiMagang</p>
            </div>
        </div>
        <div class="divider-custom">
            <div class="divider-custom-icon">
                <p class="masthead-subheading font-weight-light mb-0 text-center text-white">2</p>
                <p class="masthead-subheading font-weight-light mb-0 text-white">Membuka fitur Formulir Pengajuan</p>
            </div>
        </div>
        <div class="divider-custom">
            <div class="divider-custom-icon">
                <p class="masthead-subheading font-weight-light mb-0 text-center text-white">3</p>
                <p class="masthead-subheading font-weight-light mb-0 text-white">Mengisi Formulir Pengajuan dan
                    mengunggah file Surat Pengajuan</p>
            </div>
        </div>
        <div class="divider-custom">
            <div class="divider-custom-icon">
                <p class="masthead-subheading font-weight-light mb-0 text-center text-white">4</p>
                <p class="masthead-subheading font-weight-light mb-0 text-white">Menunggu keputusan penerimaan magang
                </p>
            </div>
        </div>
        <div class="divider-custom">
            <div class="divider-custom-icon">
                <p class="masthead-subheading font-weight-light mb-0 text-center text-white">5</p>
                <p class="masthead-subheading font-weight-light mb-0 text-white">Mengisi pendataan jika telah diterima
                    magang</p>
            </div>
        </div>
        {{-- </section> --}}
    </div>
</header>
@endsection
