@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="color:black">{{ __('Form Pendataan') }}</div>

                        <div class="card-body">
                            {{-- {{dd($data->id_pengguna)}} --}}
                            <form method="post" action="{{ route('magang.update', $data->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                @if($data['jumlah_anggota']==1)
                                <div id="konten-pendataan">
                                    <div class="form-group row">
                                        <label for="nama_ketua" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nama Ketua Kelompok') }}</label>

                                        <div class="col-md-6">
                                            <input id="nama_ketua" type="text" class="form-control" name="nama_ketua"
                                                value="{{$data['nama_ketua']}}" readonly>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="instansi_pendidikan" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Instansi Pendidikan') }}</label>

                                        <div class="col-md-6">
                                            <input id="instansi_pendidikan" type="text" class="form-control"
                                                name="instansi_pendidikan" value="{{$data['instansi_pendidikan']}}" readonly>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('E-mail Ketua Kelompok') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control"
                                    name="email" value="{{auth()->user()->email}}" readonly>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_telp" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon Ketua Kelompok') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp" type="text" class="form-control"
                                        name="no_telp" value="{{$data['no_telp']}}" required>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="durasi_magang" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Durasi Magang') }}</label>

                                    <div class="col-md-4">
                                        <input min="1" id="durasi_magang" type="number" class="form-control"
                                            name="durasi_magang" value="{{$data['durasi_magang']}}" required>
                                    </div>
                                    <div class="col-md-2">
                                        <p class="col-form-label" style="color:black">Bulan</p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="diterima_biro" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Diterima Di Biro') }}</label>

                                    <div class="col-md-6">
                                        <input id="diterima_biro" type="text" class="form-control" name="diterima_biro"
                                            @if($data['id_biro']==1) value="Biro Kepegawaian"
                                            @elseif($data['id_biro']==2) value="Biro Hubungan Masyarakat"
                                            @elseif($data['id_biro']==3) value="Biro Perencanaan"
                                            @elseif($data['id_biro']==4) value="Biro Keuangan"
                                            @elseif($data['id_biro']==5) value="Biro Umum" @endif readonly>

                                    </div>
                                </div>
                                @elseif($data['jumlah_anggota']==2)
                                <div id="konten-pendataan">
                                    <div class="form-group row">
                                        <label for="nama_ketua" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nama Ketua Kelompok') }}</label>

                                        <div class="col-md-6">
                                            <input id="nama_ketua" type="text" class="form-control" name="nama_ketua"
                                                value="{{$data['nama_ketua']}}" readonly>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="instansi_pendidikan" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Instansi Pendidikan') }}</label>

                                        <div class="col-md-6">
                                            <input id="instansi_pendidikan" type="text" class="form-control"
                                                name="instansi_pendidikan" value="{{$data['instansi_pendidikan']}}" readonly>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('E-mail Ketua Kelompok') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control"
                                    name="email" value="{{auth()->user()->email}}" readonly>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_telp" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon Ketua Kelompok') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp" type="text" class="form-control"
                                            name="no_telp" value="{{$data['no_telp']}}" required>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="durasi_magang" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Durasi Magang') }}</label>

                                    <div class="col-md-4">
                                        <input min="1" id="durasi_magang" type="number" class="form-control"
                                            name="durasi_magang" value="{{$data['durasi_magang']}}" required>
                                    </div>
                                    <div class="col-md-2">
                                        <p class="col-form-label" style="color:black">Bulan</p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="diterima_biro" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Diterima Di Biro') }}</label>

                                    <div class="col-md-6">
                                        <input id="diterima_biro" type="text" class="form-control" name="diterima_biro"
                                            @if($data['id_biro']==1) value="Biro Kepegawaian"
                                            @elseif($data['id_biro']==2) value="Biro Hubungan Masyarakat"
                                            @elseif($data['id_biro']==3) value="Biro Perencanaan"
                                            @elseif($data['id_biro']==4) value="Biro Keuangan"
                                            @elseif($data['id_biro']==5) value="Biro Umum" @endif readonly>

                                    </div>
                                </div>
                                <hr>
                                <div id="konten-pendataan">
                                    <div class="form-group row">
                                        <label for="nama_anggota1" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nama Anggota 1') }}</label>

                                        <div class="col-md-6">
                                            <input id="nama_anggota1" type="text" class="form-control"
                                             name="nama_anggota1" value="{{$data['nama_anggota1']}}" required>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email1" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('E-mail') }}</label>

                                        <div class="col-md-6">
                                            <input id="email1" type="email" class="form-control"
                                                name="email1" value="{{$data['email1']}}" required>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_telp1" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nomor Telpon/Hp') }}</label>

                                        <div class="col-md-6">
                                            <input id="no_telp1" type="text" class="form-control"
                                                name="no_telp1" value="{{$data['no_telp1']}}" required>

                                        </div>
                                    </div>
                                </div>
                                @elseif($data['jumlah_anggota']==3)
                                <div id="konten-pendataan">
                                    <div class="form-group row">
                                        <label for="nama_ketua" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nama Ketua Kelompok') }}</label>

                                        <div class="col-md-6">
                                            <input id="nama_ketua" type="text" class="form-control" name="nama_ketua"
                                                value="{{$data['nama_ketua']}}" readonly>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="instansi_pendidikan" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Instansi Pendidikan') }}</label>

                                        <div class="col-md-6">
                                            <input id="instansi_pendidikan" type="text" class="form-control"
                                                name="instansi_pendidikan" value="{{$data['instansi_pendidikan']}}" readonly>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('E-mail Ketua Kelompok') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control"
                                    name="email" value="{{auth()->user()->email}}" readonly>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_telp" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon Ketua Kelompok') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp" type="text" class="form-control"
                                            name="no_telp" value="{{$data['no_telp']}}" required>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="durasi_magang" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Durasi Magang') }}</label>

                                    <div class="col-md-4">
                                        <input min="1" id="durasi_magang" type="number" class="form-control"
                                            name="durasi_magang" value="{{$data['durasi_magang']}}" required>
                                    </div>
                                    <div class="col-md-2">
                                        <p class="col-form-label" style="color:black">Bulan</p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="diterima_biro" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Diterima Di Biro') }}</label>

                                    <div class="col-md-6">
                                        <input id="diterima_biro" type="text" class="form-control" name="diterima_biro"
                                            @if($data['id_biro']==1) value="Biro Kepegawaian"
                                            @elseif($data['id_biro']==2) value="Biro Hubungan Masyarakat"
                                            @elseif($data['id_biro']==3) value="Biro Perencanaan"
                                            @elseif($data['id_biro']==4) value="Biro Keuangan"
                                            @elseif($data['id_biro']==5) value="Biro Umum" @endif readonly>

                                    </div>
                                </div>
                                <hr>
                                <div id="konten-pendataan">
                                    <div class="form-group row">
                                        <label for="nama_anggota1" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nama Anggota 1') }}</label>

                                        <div class="col-md-6">
                                            <input id="nama_anggota1" type="text" class="form-control"
                                            name="nama_anggota1" value="{{$data['nama_anggota1']}}" required>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email1" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('E-mail Anggota 1') }}</label>

                                        <div class="col-md-6">
                                            <input id="email1" type="email" class="form-control"
                                                name="email1" value="{{$data['email1']}}" required>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_telp1" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nomor Telpon Anggota 1') }}</label>

                                        <div class="col-md-6">
                                            <input id="no_telp1" type="text" class="form-control"
                                                name="no_telp1" value="{{$data['no_telp1']}}" required>

                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div id="konten-pendataan">
                                    <div class="form-group row">
                                        <label for="nama_anggota2" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nama Anggota 2') }}</label>

                                        <div class="col-md-6">
                                            <input id="nama_anggota2" type="text" class="form-control"
                                            name="nama_anggota2" value="{{$data['nama_anggota2']}}" required>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email2" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('E-mail Anggota 2') }}</label>

                                        <div class="col-md-6">
                                            <input id="email2" type="email" class="form-control"
                                                name="email2" value="{{$data['email2']}}" required>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_telp2" class="col-md-4 col-form-label text-md-right"
                                            style="color:black">{{ __('Nomor Telpon Anggota 2') }}</label>

                                        <div class="col-md-6">
                                            <input id="no_telp2" type="text" class="form-control"
                                                name="no_telp2" value="{{$data['no_telp2']}}" required>

                                        </div>
                                    </div>
                                </div>
                                @endif


                                <div id="submit-pendataan">
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-3">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Ubah') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</header>



@endsection
