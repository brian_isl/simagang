@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <div class="container-fluid">

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Perkembangan Pengajuan</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nama Pengaju</th>
                                    <th>Instansi Pendidikan</th>
                                    <th>Status</th>
                                    <th>Lihat File</th>
                                    <th>Ubah</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nama Pengaju</th>
                                    <th>Instansi Pendidikan</th>
                                    <th>Status</th>
                                    <th>Lihat File</th>
                                    <th>Ubah</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($pengajuan as $pengajuan)
                                <tr>
                                    <td>{{$pengajuan['nama_pengaju']}}</td>
                                    <td>{{$pengajuan['asal_surat']}}</td>
                                <td>{{$pengajuan->keterangan}}</td>
                                    <td>
                                        <a href="{{route('open_file', [$pengajuan['id']])}}">
                                            <button type="button" class="btn btn-primary btn-user btn-block">
                                                Lihat File
                                            </button>
                                        </a>
                                    </td>
                                    @if($pengajuan['status']==1)
                                    <td>
                                        <a href="{{route('pengajuan.edit', [$pengajuan['id']])}}">
                                            <button type="button" class="btn btn-primary btn-user btn-block">
                                                Ubah
                                            </button>
                                        </a>
                                    </td>
                                    @elseif($pengajuan['status']==0)
                                    <td>
                                        <a href="{{route('error_ditolak')}}">
                                            <button type="button" class="btn btn-primary btn-user btn-block">
                                                Ubah
                                            </button>
                                        </a>
                                    </td>
                                    @elseif($pengajuan['status']==2)
                                    <td>
                                        <a href="{{route('error_pengajuan_diproses')}}">
                                            <button type="button" class="btn btn-primary btn-user btn-block">
                                                Ubah
                                            </button>
                                        </a>
                                    </td>
                                    @else
                                    <td>
                                        <a href="{{route('error_diterima')}}">
                                            <button type="button" class="btn btn-primary btn-user btn-block">
                                                Ubah
                                            </button>
                                        </a>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
