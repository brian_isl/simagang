@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="color:black">{{ __('Pengisian Logbook Magang') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{route('logbook.store')}}" enctype="multipart/form-data">
                                @csrf
                                @method('POST')
                                <div class="form-group row">
                                    <label for="tanggal" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Tanggal') }}</label>

                                    <div class="col-md-6">
                                        <input id="tanggal" type="date" class="form-control" name="tanggal"
                                            required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lokasi" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Lokasi') }}</label>

                                    <div class="col-md-6">
                                        <input id="lokasi" type="text" class="form-control"
                                            name="lokasi" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="keterangan" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Keterangan') }}</label>

                                    <div class="col-md-6">
                                        <input id="keterangan" type="text" class="form-control"
                                            name="keterangan" required>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Tambah Data') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@endsection
