@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')
@if (session('status'))
{{ session('status') }}
@endif
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="color:black">{{ __('Form Pengajuan') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('pengajuan.store') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="nama_pengaju" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nama Pengaju') }}</label>

                                    <div class="col-md-6">
                                        <input id="nama_pengaju" type="text" class="form-control" name="nama_pengaju"
                                            value="{{auth()->user()->name}}" readonly>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_induk" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Induk Pengaju') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_induk" type="text" class="form-control"
                                            name="no_induk" required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('E-mail') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="text" class="form-control" name="email"
                                            value="{{auth()->user()->email}}" readonly>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_telp" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp" type="number" class="form-control" name="no_telp" required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="instansi_pendidikan" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Instansi Pendidikan') }}</label>

                                    <div class="col-md-6">
                                        <input id="instansi_pendidikan" type="text" class="form-control"
                                            name="instansi_pendidikan" required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="jurusan" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Jurusan') }}</label>

                                    <div class="col-md-6">
                                        <input id="jurusan" type="text" class="form-control"
                                            name="jurusan" required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="mulai_magang" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Mulai Magang') }}</label>

                                        <div class="col-md-6">
                                            <input id="mulai_magang" type="date" class="form-control" name="mulai_magang"
                                                required>

                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="selesai_magang" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Selesai Magang') }}</label>

                                        <div class="col-md-6">
                                            <input id="selesai_magang" type="date" class="form-control" name="selesai_magang"
                                                required>

                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nama_anggota1" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nama Anggota 1') }}</label>

                                    <div class="col-md-6">
                                        <input id="nama_anggota1" type="text" class="form-control" name="nama_anggota1">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_induk1" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Induk Anggota 1') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_induk1" type="text" class="form-control"
                                            name="no_induk1">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email1" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('E-mail Anggota 1') }}</label>

                                    <div class="col-md-6">
                                        <input id="email1" type="email" class="form-control"
                                            name="email1">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_telp1" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon Anggota 1') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp1" type="text" class="form-control"
                                            name="no_telp1">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="nama_anggota2" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nama Anggota 2') }}</label>

                                    <div class="col-md-6">
                                        <input id="nama_anggota2" type="text" class="form-control" name="nama_anggota2">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_induk2" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Induk Anggota 2') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_induk2" type="text" class="form-control"
                                            name="no_induk2">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email2" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('E-mail Anggota 2') }}</label>

                                    <div class="col-md-6">
                                        <input id="email2" type="email" class="form-control"
                                            name="email2">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_telp2" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon Anggota 2') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp2" type="text" class="form-control"
                                            name="no_telp2">

                                    </div>
                                </div>
                                {{--
                                <div class="form-group row">
                                    <label for="jumlah_anggota" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Jumlah Anggota') }}</label>

                                <div class="col-md-6">
                                    <select name="jumlah_anggota" id="jumlah_anggota"
                                        class="form-control form-control-user" required>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="file_pengajuan" class="col-md-4 col-form-label text-md-right"
                                style="color:black">{{__('Unggah File (Pdf)')}}</label>
                            <div class="col-md-3 text-md-left">
                                <input type="file" id="file_pengajuan" name="file_pengajuan" style="color:black"
                                    accept="application/pdf" required />
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Ajukan') }}
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</header>

@endsection
