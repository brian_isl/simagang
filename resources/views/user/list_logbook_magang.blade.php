@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <div class="container-fluid">

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Logbook Magang</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Lokasi</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Ubah</th>
                                    {{-- <th>Hapus</th> --}}
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Lokasi</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Ubah</th>
                                    {{-- <th>Hapus</th> --}}
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($logbook as $logbook)
                                <tr>
                                    <td>{{$logbook->tanggal}}</td>
                                    <td>{{$logbook->lokasi}}</td>
                                    <td>{{$logbook->keterangan}}</td>
                                    @if ($logbook->status==1)
                                    <td>Menunggu Verifikasi Pembimbing BKN</td>
                                    @elseif($logbook->status==2)
                                    <td>Diverifikasi</td>
                                    @elseif($logbook->status==0)
                                    <td>Ditolak</td>
                                    @endif
                                    <td>
                                        <form action="{{route('logbook.edit', $logbook->id)}}" method="get">
                                            <button type="submit"
                                                class="btn btn-primary btn-user btn-block">Ubah</button>
                                        </form>
                                    </td>
                                    {{-- @if($perkembangan->status==2)
                                    <td></td>
                                    @else
                                    <td>
                                        <form action="#{{--route('pengajuan.destroy', [$pengajuan['id']])--}}"
                                            {{-- method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-user btn-block">
                                                Hapus
                                            </button>
                                        </form>
                                    </td>
                                    @endif --}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <a href="{{route('logbook.create')}}">
                        <button type="button" class="btn btn-primary btn-user btn-block">
                            Tambah Logbook Magang
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

@endsection
