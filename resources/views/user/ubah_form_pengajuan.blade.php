@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')
@if (session('status'))
{{ session('status') }}
@endif
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="color:black">{{ __('Form Pengajuan') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('pengajuan.update', $pengajuan->id) }}"
                                enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group row">
                                    <label for="nama_pengaju" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nama Pengaju') }}</label>

                                    <div class="col-md-6">
                                        <input id="nama_pengaju" type="text" class="form-control" name="nama_pengaju"
                                            value="{{$pengajuan['nama_pengaju']}}" readonly>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_induk" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Induk Pengaju') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_induk" type="text" class="form-control" name="no_induk"
                                            value="{{$pengajuan['no_induk']}}" required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Email') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="text" class="form-control" name="email"
                                            value="{{auth()->user()->email}}" readonly>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_telp" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp" type="number" class="form-control" name="no_telp"
                                    value="{{$pengajuan['no_telp']}}" required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="instansi_pendidikan" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Instansi Pendidikan') }}</label>

                                    <div class="col-md-6">
                                        <input id="instansi_pendidikan" type="text" class="form-control"
                                            name="instansi_pendidikan" value="{{$pengajuan['asal_surat']}}" required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="jurusan" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Jurusan') }}</label>

                                    <div class="col-md-6">
                                        <input id="jurusan" type="text" class="form-control"
                                            name="jurusan" value="{{$pengajuan['jurusan']}}" required>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="mulai_magang" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Mulai Magang') }}</label>

                                        <div class="col-md-6">
                                            <input id="mulai_magang" type="date" class="form-control" name="mulai_magang"
                                            value="{{$pengajuan['mulai_magang']}}" required>

                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="selesai_magang" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Selesai Magang') }}</label>

                                        <div class="col-md-6">
                                            <input id="selesai_magang" type="date" class="form-control" name="selesai_magang"
                                            value="{{$pengajuan['selesai_magang']}}" required>

                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nama_anggota1" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nama Anggota 1') }}</label>

                                    <div class="col-md-6">
                                        <input id="nama_anggota1" type="text" class="form-control" name="nama_anggota1"
                                        value="{{$pengajuan['nama_anggota1']}}">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_induk1" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Induk Anggota 1') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_induk1" type="text" class="form-control"
                                            name="no_induk1" value="{{$pengajuan['no_induk1']}}">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email1" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('E-mail Anggota 1') }}</label>

                                    <div class="col-md-6">
                                        <input id="email1" type="email" class="form-control"
                                            name="email1" value="{{$pengajuan['email1']}}">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_telp1" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon Anggota 1') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp1" type="text" class="form-control"
                                            name="no_telp1" value="{{$pengajuan['no_telp1']}}">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="nama_anggota2" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nama Anggota 2') }}</label>

                                    <div class="col-md-6">
                                        <input id="nama_anggota2" type="text" class="form-control" name="nama_anggota2"
                                        value="{{$pengajuan['nama_anggota2']}}">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="no_induk2" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Induk Anggota 2') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_induk2" type="text" class="form-control"
                                            name="no_induk2" value="{{$pengajuan['no_induk2']}}">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email2" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('E-mail Anggota 2') }}</label>

                                    <div class="col-md-6">
                                        <input id="email2" type="email" class="form-control"
                                            name="email2" value="{{$pengajuan['email2']}}">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_telp2" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{ __('Nomor Telpon Anggota 2') }}</label>

                                    <div class="col-md-6">
                                        <input id="no_telp2" type="text" class="form-control"
                                            name="no_telp2" value="{{$pengajuan['no_telp2']}}">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="file_pengajuan" class="col-md-4 col-form-label text-md-right"
                                        style="color:black">{{__('Unggah File (Pdf)')}}</label>
                                    <div class="col-md-3 text-md-left">
                                        <input type="file" id="file_pengajuan" name="file_pengajuan" style="color:black"
                                            required />
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Ubah Data') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@endsection
