@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <div class="container-fluid">

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Magang</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Biro</th>
                                    <th>Instansi Pendidikan</th>
                                    <th>Nama Ketua</th>
                                    <th>E-Mail</th>
                                    <th>Nomor Telpon</th>
                                    <th>Nama Anggota 1</th>
                                    <th>E-Mail</th>
                                    <th>Nomor Telpon</th>
                                    <th>Nama Anggota 2</th>
                                    <th>E-Mail</th>
                                    <th>Nomor Telpon</th>
                                    <th>Nilai Akhir</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Biro</th>
                                    <th>Instansi Pendidikan</th>
                                    <th>Nama Ketua</th>
                                    <th>E-Mail</th>
                                    <th>Nomor Telpon</th>
                                    <th>Nama Anggota 1</th>
                                    <th>E-Mail</th>
                                    <th>Nomor Telpon</th>
                                    <th>Nama Anggota 2</th>
                                    <th>E-Mail</th>
                                    <th>Nomor Telpon</th>
                                    <th>Nilai Akhir</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    @if($magang->magang_pengajuan->teruskan_biro==1)
                                    <td>Biro Kepegawaian</td>
                                    @elseif($magang->magang_pengajuan->teruskan_biro==2)
                                    <td>Biro Hubungan Masyarakat</td>
                                    @elseif($magang->magang_pengajuan->teruskan_biro==3)
                                    <td>Biro Perencanaan</td>
                                    @elseif($magang->magang_pengajuan->teruskan_biro==4)
                                    <td>Biro Keuangan</td>
                                    @elseif($magang->magang_pengajuan->teruskan_biro==5)
                                    <td>Biro Umum</td>
                                    @endif
                                <td>{{$magang->magang_pengajuan->asal_surat}}</td>
                                <td>{{$magang->magang_pengajuan->nama_pengaju}}</td>
                                <td>{{$magang->magang_pengajuan->email}}</td>
                                <td>{{$magang->magang_pengajuan->no_telp}}</td>
                                <td>{{$magang->magang_pengajuan->nama_anggota1}}</td>
                                <td>{{$magang->magang_pengajuan->email1}}</td>
                                <td>{{$magang->magang_pengajuan->no_telp1}}</td>
                                <td>{{$magang->magang_pengajuan->nama_anggota2}}</td>
                                <td>{{$magang->magang_pengajuan->email2}}</td>
                                <td>{{$magang->magang_pengajuan->no_telp2}}</td>
                                <td>{{$magang->magang_nilai->nilai_akhir}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                </div>
            </div>
        </div>

    </div>
</header>
@endsection
