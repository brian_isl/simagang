@section('bottombar')
<!-- Footer -->
<footer class="footer text-center">
    <div class="container">
        <div class="row">

            <!-- Footer Location -->
            <div class="col-lg-6 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Lokasi</h4>
                <p class="lead mb-0">Jl. Mayjen Sutoyo No. 12
                    <br>Jakarta Timur 13640
                    <br> Telp 021-8093008</p>
            </div>

            <!-- Footer Social Icons -->
            <div class="col-lg-6 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Media Sosial</h4>
                <a class="btn btn-outline-light btn-social mx-1" href="https://www.facebook.com/BKNgoid/">
                    <i class="fab fa-fw fa-facebook-f"></i>
                </a>
                <a class="btn btn-outline-light btn-social mx-1" href="https://twitter.com/BKNgoid?s=09">
                    <i class="fab fa-fw fa-twitter"></i>
                </a>
                <a class="btn btn-outline-light btn-social mx-1" href="https://www.bkn.go.id">
                    <i class="fab fa-fw fa-dribbble"></i>
                </a>
            </div>



        </div>
    </div>
</footer>
@endsection
