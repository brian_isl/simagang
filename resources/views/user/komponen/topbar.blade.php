@section('topbar')


<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{route('home')}}">SiMagang</a> <button
            aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"
            class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded"
            data-target="#navbarResponsive" data-toggle="collapse" type="button">Menu <i
                class="fas fa-bars"></i></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li style="list-style: none">
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="{{route('alur_pengajuan')}}">Alur Pengajuan</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="{{route('tentang_simagang')}}">Tentang SiMagang</a>
                </li>
                @guest
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                <li style="list-style: none">@if (Route::has('register'))</li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="{{ route('register') }}">{{ __('Registrasi') }}</a>
                </li>
                @endif
                @else

                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link py-3 px-0 px-lg-3 dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a href="{{route('pengajuan.create')}}" class="dropdown-item">{{__('Formulir Pengajuan')}}</a>
                        <a href="{{route('pengajuan.index')}}" class="dropdown-item">{{__('Lihat Pengajuan')}}</a>
                        <a href="{{route('magang.index')}}" class="dropdown-item">{{__('Data Magang')}}</a>
                        <a href="{{route('logbook.index')}}" class="dropdown-item">{{__('Logbook Magang')}}</a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>


                    </div>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

@endsection
