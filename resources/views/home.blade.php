@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')

<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        @if (session('status'))
        {{ session('status') }}
        @endif
        <!-- Masthead Avatar Image -->
        {{-- <img class="masthead-avatar mb-5" height="300px" src="{{asset('freelancer/img/logo_transparent.png')}}"
        width="150px">
        <!-- Masthead Heading --> --}}
        <h1 class="masthead-heading mb-0">Selamat Datang</h1>
        <h1 class="masthead-heading mb-0">di</h1>
        <br><br>
        <h1 class="masthead-heading mb-0">SiMagang</h1>
        <h1 class="masthead-heading mb-0">Badan Kepegawaian Negara</h1><!-- Icon Divider -->
        <div class="divider-custom divider-light">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon">
                <i class="fas fa-star"></i>
            </div>
            <div class="divider-custom-line"></div>
        </div>
    </div>
</header><!-- About Section -->

</div>
</header>


@endsection
