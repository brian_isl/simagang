@extends('layouts.app')
@extends('user.komponen.bottombar')
@extends('user.komponen.topbar')

@section('content')

<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="color:black">Anda Sudah Diterima Sebagai Magang!</div>

                        <div class="card-body" style="color:black">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            Status anda sudah diterima sebagai siswa magang BKN
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@endsection
